import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

export const firebaseConfig = {
  apiKey: 'AIzaSyDYQUyjFapwkTcxguiwkHpWKpmIXZw_qAU',
  authDomain: 'aggro-audience-forum.firebaseapp.com',
  databaseURL: 'https://aggro-audience-forum-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'aggro-audience-forum',
  storageBucket: 'aggro-audience-forum.appspot.com',
  messagingSenderId: '464836828455',
  appId: '1:464836828455:web:2a79b99fc58d734063dd02'
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
